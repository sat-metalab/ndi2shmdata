# How to make new releases

First make sure `develop` branch reflects the desired `master`:
* create the release branch from develop: `git checkout develop && git checkout -b current_release`
* update AUTHORS.md
* Increase the version (major, minor or patch) or in the CMakeLists.txt file located at the root of the project 
* Create a new entry in the NEWS.md file (top of the file) with version number, date and description of changes
* Commit and push, open a code review

Release into the master branch:
* remove release branch: `git checkout develop && git pull origin develop && git branch -d current_release`
* merge into master: `git checkout master && git pull origin master && git merge --no-ff develop`
* tag the version `git tag -a vX.X.X -m"Short message that explains the new version"`
* push to master: `git push origin master --tags`
* rebase develop on master:`git checkout develop && git rebase master && git push origin develop`

You're done.
