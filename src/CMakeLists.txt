configure_file(
    "ndi2shmdata.in"
    "ndi2shmdata"
    IMMEDIATE @ONLY)

configure_file(
    "shmdata2ndi.in"
    "shmdata2ndi"
    IMMEDIATE @ONLY)

add_executable(shmdatatondi
  shmdata2ndi.cpp
)

add_executable(nditoshmdata
  ndi2shmdata.cpp
  )

# INSTALL

install(TARGETS shmdatatondi nditoshmdata
  RUNTIME DESTINATION nditoshmdata/bin
  COMPONENT applications
  )

install(FILES
  ${NDI_ARCH_LIB_DIR}/libndi.so
  ${NDI_ARCH_LIB_DIR}/libndi.so.6
  ${NDI_ARCH_LIB_DIR}/libndi.so.6.0.0
  DESTINATION nditoshmdata/lib )

install(FILES
  ${CMAKE_CURRENT_BINARY_DIR}/ndi2shmdata 
  ${CMAKE_CURRENT_BINARY_DIR}/shmdata2ndi
  DESTINATION bin
  PERMISSIONS WORLD_READ WORLD_EXECUTE
)

